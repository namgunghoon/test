function test2(participant, completion) {
    participant.sort()
    completion.sort()

    return participant.find((e, i) => {
       if(e !== completion[i]){
           return e
       }
    });
}